# Introduction

JAliEn is the next generation ALICE Grid middleware for Run 3.
The software is available in CVMFS and alidist. All ALICE users are encouraged to migrate. Classic AliEn is no longer updated and will be discontinued toward February 2022.

See the [migration guidelines](user/migration) and [tutorials](user/tutorials) for more information.
Latest presentation on status of ```alien.py``` client can be seen [here](https://docs.google.com/presentation/d/1HrJDhKw8WSP4cuDLCNDo8Z9_ZV0rnIe6DM58dpyxDag/)

The front-facing JAliEn web site is [alien.web.cern.ch](https://alien.web.cern.ch) where you can find instructions on how to [register with the ALICE VO](https://alien.web.cern.ch/content/vo/alice/userregistration).

You can improve this documentation by editing files in the GitLab project [jalien/jalien-docs](https://gitlab.cern.ch/jalien/jalien-docs).
