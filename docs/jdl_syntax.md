# JDL syntax reference

## Different split options
Splitting a job into smaller subjobs is based on the strategy defined in the JDL and will split
files provided by InputData or InputCollection to different subjobs with the same executable.
Different strategies have different optional or mandatory fields.


### Split
``` 
    Will only split the job if this field is defined.    

    usage: Split="[strategy]"
    
```

---

### SplitArguments
``` 
    Reduntant field, but this field will be added to Arguments for all subjobs

    usage: SplitArguments="[arguments for executable]"
    
```

---

## **Split strategies options**
[production](/user/jdlSplit/production)  
[file](/user/jdlSplit/file)  
[directory](/user/jdlSplit/directory)  
[parentdirectory](/user/jdlSplit/parentdirectory)  
[se](/user/jdlSplit/se)  
[af](/user/jdlSplit/af)  
[directaccess](/user/jdlSplit/directaccess)

## Other split arguments ##

### SplitMaxInputFileNumber
``` 
    Sets a maximum limit for number of inputdata files per subjob

    usage: SplitMaxInputFileNumber="[number]"
    
    
```

---
### SplitMaxInputFileSize
``` 
    Sets a maximum limit for combined size of inputdata files per subjob

    usage: SplitMaxInputFileSize="[number]"
    
    
```

---

### SplitMinInputFileNumber
``` 
    Sets a minimum limit for number of inputdata files per subjob, used by storage element split
    to merge subjobs with less inputdata files than the limit 

    usage: SplitMinInputFileNumber="[number]"
    
    
```

---

### ForceOnlySEInput
``` 
    Used by Analysis Facility to force only inputdata files located on site provided in Requirements of JDL to be used.
    Other files are ignored for the job. Has a default threshhold of missing files before it fails.

    usage: ForceOnlySEInput="[true/false]"
    
    
```

---

### MaxInputMissingThreshold
``` 
    Sets a percentage value of missing files from the SE before an af split fails. Only used with af split

    usage: MaxInputMissingThreshold="[percentage]"    
    
```

---

###  OrderLFN
``` 
    Order inputdata files in the JDL based on a given strategy (Usually will already be alphabetical by default)

    usage: OrderLFN = "options"
    
    options:
           random --> Shuffle all files randomly
           size --> Order by size, matching largest with smalles and so forth
           alphabetical --> order by name
           "any string" --> String needs to be followed by numbers that is the basis of the sorting.
                            Examples for usae is: epn or tf           
    
```

---

## **#alien# pattern**

This pattern is replaced by a value based on input data or a counter in the final JDL. This could be used anywhere
in the JDL but is often used in Arguments or Outputs. 

Examples: OutputDir = "/alice/data/2024/LHC24an/556767/cpass0/0200/#alienfilename/.root//#";\
          OutputDir = "/alice/data/2024/LHC24an/556767/cpass0/0200/#alien_counter_03i#";
          

###  counter
``` 
    An increasing subjob counter, can define 

    usage: #alien_counter# --> 1,2,3....
    
    options:
           #alien_counter_[number of digits]i# --> #alien_counter_03i# = 001, 002, 003... 
    
    
```

---

###  file patterns
``` 
    Replace this pattern with a value based on either the first or last of the inputdata files in the subjob. 
    Default if not provided is first.

    usage: #alien[first/last][option]# 
    
    options:
           dir --> /alice/cern.ch/user/a/alice/LHC22f3.xml = alice
           fulldir --> /alice/cern.ch/user/a/alice/LHC22f3.xml = /alice/cern.ch/user/a/alice/LHC22f3.xml
           filename/[pattern to be replaced]/[new value] --> filename/.xml/.new/ --> /alice/cern.ch/user/a/alice/LHC22f3.xml= LHC22f3.new
           
    example:
            #alienlastdir#
            #alienfilename/.root//#
    
    
```

---
