# XRootD Installation (RPMs)

The following documentation describes how to install XRootD using RPMs.
<br>The old page (with legacy instructions) can be found [here](https://alien.web.cern.ch/content/documentation/howto/site/xrootdbyrpms)

!!! question ""
    For questions and support, please contact [Adrian Sevcenco](../../user/help/#contact-us).

## Packages Installation

!!! info "Prerequisites"
    These instructions assume EL7/CC7 x86_64 platform that is supported and tested. 
    <br>__Redirector on disk server configuration is not supported nor foreseen.__


!!! danger "Preparation of storage locations"
    It is assumed that the server is already prepared with the necessary directories and correct rights and ownership.


1. Install required tools (__root user is assumed__):

    ```bash
    ~# yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    ```

2. Install the WLCG repository, for example for CC7:

    ```bash
    ~# rpm --import http://linuxsoft.cern.ch/wlcg/RPM-GPG-KEY-wlcg

    ~# yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm
    ```

!!! info "Updating from XRootD v4 to v5 case"
    Because of configuration fixes and not only the minimum version is 5.1 (but recommended to use latest version available)
    See upgrading below


3. Install the required packages  
    3.1 If fresh install, install the metapackage that will pull XRootD as a dependency (when present in epel):

    ```bash
    ~# yum -y install alicexrdplugins
    ```

    3.2 For upgrade from v4 case it's best to remove all XRootD packages

    ```bash
    ~# yum remove -y xrootd*
    ```

    Then one can install either the epel version or the upstream XRootD version (N.B.!! epel have no downgrade path)
    <br>(see Yum repositories section from [XRootD downloads](https://xrootd.slac.stanford.edu/dload.html))
    ```bash
    ~# yum install -y xrootd-client xrootd-server
    ```

    The ALICE XRootD plugins are:  
        - `alicetokenacc` (dependent on XRootD)  
        - `xrootd-aggregatingname2name` (dependent on XRootD)  
        - `alicexrdplugins` (metapackage, will pull the above packages)  

    Until these pluggins are accepted in WLCG repository, `alicetokenacc`, `xrootd-aggregatingname2name`, `alicexrdplugins` can be downloaded from [here](https://asevcenc.web.cern.ch/asevcenc/xrootd5alice/)
    otherwise they are usually found in WLCG repository. (chose the relevant distro, for centos 7, chose `el7` directory.)


## XRootD Management Script

!!! warning "Change of management tools!!! (and configuration template)"
    The former `xrd.sh` was re-wrote and splited in `xrdctl` service management script and `xrd_mk_config` configuration generation script
    <br>See the github repository [alicexrd](https://github.com/adriansev/alicexrd)
    <br>When upgrading recreate `system.cnf` file (it has new content/organization) and run `xrd_mk_config` for XRootD configuration generation.


!!! warning ""
    Tested: __Running of XRootD as non-privileged user__
    <br>Not yet tested: use of `xrd_mk_config` generated systemd service files.

1. Deployment and setting up the environment:
    <br>`xrdctl` and `xrd_mk_config` scripts and configuration files can be automatically deployed from github as follows:

    ```bash
    ~$ bash <(curl -fsSLk https://raw.githubusercontent.com/adriansev/alicexrd/master/deploy_alicexrd)
    ```
    The script will place the scripts within `${XRDCTLDIR:=~/alicexrd}` and configurations in `${XRDCONFDIR:=~/alicexrdconf}`

    Within `XRDCONFDIR`:
    <br>```~$ cp system.cnf_CHANGEME system.cnf```
    <br> Then set `SE_NAME, LOCALROOT, OSS_SPACE_DIRS` as all former others are detected automatically.


2. After completing the [configuration](#script-configuration) run the script: ```$XRDCTLDIR/xrdctl -c```.
    <br>(one can symlink `xrdctl` into `$HOME/bin/`)
    <br>Usage: ```xrdctl arg [configuration_file]```, where argument is _one_ of :

    | Argument | Description |
    |----------|-------------|
    | `-l | --l | -list | --list` | list found configuration files
    | `-s | --s | -status | --status` | show status of services
    | `-c | --c | -check | --check` | check and restart if not running
    | `-k | --k | -kill | --kill` | kill running processes
    | `-f | --f | -restart | --restart` | force restart
    | `-logs | --logs` | manage the logs
    | `-addcron | --addcron` | add/refresh cron line
    | `-removecron | --removecron` | remove the cron line

    optionaly the configuration file can be explicitly declared.

    !!! note
        The operations will be applied on  
            - specified configuration file  
            __OR__  
            - on *ALL* `.xrdcfg` files from `$XRDCONFDIR`  

    Environment variables for `xrdctl`:

    | Argument | Description |
    |----------|-------------|
    | XRD_READONLY | checked by xrootd configuration; if set the export (__for ALL found configurations__) will be declared as notwritable
    | XRDCTL_CMD_XRD/XRDCTL_CMD_CMSD | xrootd and cmsd executables (other than the defaults from /usr/bin)
    | XRDCTL_PRELOAD | use LD_PRELOAD for xrootd/cmsd processes
    | XRDRUNDIR | location of admin,core,logs,logsbackup dirs; if not set it will be XRDCTLDIR/run/
    | XRDCONFDIR | location (directory) of .xrdcfg configuration file(s); if not set it wll be XRDCTLDIR/xrootd.conf/
    | XRDCTL_DEBUG | if set (any value) it will enable various printouts of xrdctl
    | XRDCTL_DEBUG_SETX | bash enable -x
    | XRDCTL_DEBUG_XRD | if set enable debug flag for xrootd process
    | XRDCTL_DEBUG_CMSD | if set enable debug flag for cmsd process


## Script Configuration

The following table highlights environment variables that influence how ```xrd_mk_config``` works:
<br>Usage: ```xrd_mk_config [template_configuration_file]```
<br>where the specified argument is a custom path for template configuration (otherwise the `${XRDCONFDIR}/xrootd.xrootd.cf.tmp` is used)
<br>It will parse/source `${XRDCONFDIR}/system.cnf` and generate the xrootd configuration.

!!! warning "`${XRD_MULTISRV}` usage"
    If "`${XRD_MULTISRV}`" is set, it will enable the spawning of an xrootd/cmsd process __FOR EACH `OSS_SPACE_DIRS`__
    <br>In this mode, `oss.space` declaration will be commented/omitted,
    and a distinct xrootd configuration file will be generated for each directory specified by `OSS_SPACE_DIRS`

!!! note ""
    `OSS_SPACE_DIRS` will be explicitly listed, so globbing will be expanded to all possible directories.
    <br>Example: `/storage*/xrddata` will be expanded by `ls` to all __existing__ directories


| Environment Variable | Description |
|----------------------|-------------|
| XRDRUNDIR | location of admin,core,logs,logsbackup dirs; if not set it will be XRDCTLDIR/run/
| XRDCONFDIR | location (directory) of .xrdcfg configuration file(s); if not set it wll be XRDCTLDIR/xrootd.conf/



## XRootD MonaLisa monitoring

The monitoring of XRootD is done by MonaLisa agent - MLSensor  
This is deployed as a distinct service, found within WLCG repository.

```bash
~# yum -y install mlsensor
```

There is a required configuration of service (as root):
```bash
~# cd /etc/mlsensor
~# mlsensor_config mlsensor.properties.tmp <name of ALICE storage name>
```

!!! warning "Multiple services/custom ports"
    if multiple XRootD services are used (or custom ports), make sure to modify `mlsensor.properties` file  
    and update `lia.Monitor.modules.monXrdSpace.args` variable to a space separrated list of XRootD ports, e.g.:  
    `lia.Monitor.modules.monXrdSpace.args=1094 1095`


then the normal system enablement procedure can take place
```
systemctl enable mlsensor
systemctl start mlsensor
```

!!! info "Support for MLSensor"
    For the package and deployment details (configuration): adrianDOTsevcencoATNOSPAMcernDOTch  
    For the monitoring mechanics and features of java package: costinDOTgrigorsATNOSPAMcernDOTch  


