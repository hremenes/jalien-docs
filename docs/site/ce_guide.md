# Computing Element Guide

This guide provides instructions on setting up JAliEn Computing Element (CE) by either running directly from CVMFS [(Option 1)](#option-1-running-from-cvmfs), or by cloning the JAliEn Git repository [(Option 2)](#option-2-compile-from-source).

## Option 1: Running from CVMFS

This approach only requires CVMFS and a valid Grid certificate or a user/host token in ```~/.globus```. It is the simplest approach for getting a CE up and running, but configuration is limited to the options given in the config files. 

1. Ensure environment path for the proxy certificate is set. See output from ```voms-proxy-info -path``` to get the full path, for example:

    ```console
    ~$ export X509_USER_PROXY='/var/lib/vobox/alice/proxy_repository/+2fDC+3dch+2fDC...'
    ```

2. Load the JAliEn environment:

    ```console
    #If not in path, can be found at `/cvmfs/alice.cern.ch/bin/alienv`.
    ~$ alienv enter JAliEn
    ```

3. Run the CE:

    ```console
    ~$ jalien alien.site.ComputingElement
    ```

This will start a JAliEn CE with default configuration options.
Override is possible by placing desired configuration in ```~/.j/config```.
Overview of the config files, and their default values, can be found [here](https://gitlab.cern.ch/jalien/jalien/-/tree/master/config).

!!! hint "User/Host token"
    User and host tokens can be generated from within a [JAliEn shell](../../jalien_commands/#token) by using ```token -t user/host``` command.
    These can be put as user[cert/key].pem into ```.globus``` folder, instead of a full Grid certificate.

## Option 2: Compile From Source

!!! info "Requirements"
    This approach requires compile tools such as JDK11+ be available.

This approach gives access to the latest **CE** changes without waiting for a new tag to be pushed to CVMFS. JobAgents will still run from CVMFS, but their startup script may now be modified - allowing for changes such as what variables are exported, scripts are run before start, and containers may also be enabled/disabled as desired. Other in-code overrides are also possible (at own risk). As with the first option, a valid Grid certificate or a user/host token in ```~/.globus``` is required.

1. Clone the JAliEn Git repository:

    ```console
    ~$ git clone https://gitlab.cern.ch/jalien/jalien.git
    ```

2. Compile JAliEn using the following commands, adjusting configuration in ```jalien/config``` as needed beforehand.

    ```console
    ~$ cd jalien
    ~$ ./compile.sh all
    ```
 
3. Ensure environment path for the proxy certificate is set. See output from ```voms-proxy-info -path``` to get the full path, for example:

    ```console
    ~$ export X509_USER_PROXY='/var/lib/vobox/alice/proxy_repository/+2fDC+3dch+2fDC...'
    ```

4. Run the CE as follows:

    ```console
    ~$ ./jalien ComputingElement
    ```

## Managing the CE

A wrapper script can be used to shortcut common tasks such as ```start```, ```stop``` and checking the service ```status```:

??? example "Example: jalienCE.sh"
    ```bash
    #!/bin/bash

    export X509_USER_PROXY=
    export ALICE_LOGDIR= #e.g ~/ALICE/alien-logs
    export JALIEN_PATH= #e.g ~/jalien (remove if running from CVMfS)

    cd $ALICE_LOGDIR

    if [ $1 = "start" ]
      then
        echo "Starting JAliEn CE"
        nohup $JALIEN_PATH/jalien ComputingElement & echo $! > CE.pid
    elif [[ $1 = "stop" ]]
      then
        echo "Stopping JAliEn CE"
        pkill -f alien.site.ComputingElement
    elif [ $1 = "status" ]
      then
       if ps -p $(cat $ALICE_LOGDIR/CE.pid) > /dev/null 2>&1
       then
          echo "JAliEn CE is running"
       else
          echo "JAliEn CE is NOT running!"
       fi 
    else 
        echo "Usage: 'start', 'stop' or 'status'" 
    fi
    ```

Be aware that the JAliEn CE will terminate once the token expires. 
To avoid downtime, a service can be set up to automatically restart it once that happens. 
A trivial solution can be to extend the above wrapper script with a restarter as follows:

??? example "Example: jalien-restarter.sh"
    ```bash
    #!/bin/bash

    JALIEN_CE_WRAPPER=~/jalienCE.sh

    while true
    do
       if [ "$($JALIEN_CE_WRAPPER status)" = "JAliEn CE is running" ]
       then
          echo "OK"
       else
          echo "Not OK"
          $($JALIEN_CE_WRAPPER start)
       fi
    sleep  1800
    done
    ```

## FAQ

**What about logs?**

The JAliEn CE log directory is defined in LDAP, the same way as for legacy AliEn. A common directory used is ```~/ALICE/alien-logs```.

**What about updates?**

When using option 1 (CE in CVMFS), the CE can be updated by simply reloading the CE and alienv, activating the latest tag. For option 2 (CE from git), the latest version can be obtained by doing a ```git pull``` and ```compile.sh all```, before restarting.

No action is needed for the JobAgents, as these are always started using the same (or closest available) tag as the CE through CVMFS.
