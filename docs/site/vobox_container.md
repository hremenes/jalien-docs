# VOBox Container

This guide describes how to create a networked **Docker** container for VO-Box use.<br>

A guide for **Apptainer** and **Podman** may now also be found [here](https://gitlab.cern.ch/jalien/container-vobox). This version can run directly from CVMFS, without the need for any additional packages on the host.

For the previous guide refer to the [this](../vobox_legacy) page.

## Requirements

| | |
|-|-|
| __CentOS__ | 7.0 or later |
| __Docker__ | 1.12 or later (Tested on 17.03.1-CE a.k.a. "1.14") |
| __CVMFS__  | Installed on the host |

## Setup Networking

Create a new MACVLAN bridge with both IPv4 and IPv6 support, named __docknet__, using the following command:

```console
# docker network create -d macvlan \
	--subnet=137.138.47.192/26 \
	--gateway=137.138.47.193 \
	--ipv6 \
	--subnet=2001:1458:201:b50e::/64 \
	--gateway=2001:1458:201:b50e::1 \
	-o parent=eth0 docknet
```

??? note "Command details"
    
    * __subnet/gateway__ must be replaced with the settings applicable to your network, while __parent__ must be the network interface.
    * Why MACVLAN? Docker's normal approach to bridging is simply a NAT translation scheme. MACVLAN bridges give the containers direct access to the network.
    * Containers on this bridge will be assigned their own MAC addresses, and appear as conventional computers on the local network.
    * However, due to the way MACVLAN works, the host will not be able to ping the containers (and vice versa).

## Create Container

1. Clone the repository containing the desired preconfigured setup: 

    | Scheduler | Command |
    |:----------|:--------|
    | __HTCondor__  | ```# git clone https://gitlab.cern.ch/mstoretv/dockervobox.git``` |
    | __ARC/Generic__ | ```# git clone https://gitlab.cern.ch/mstoretv/dockervobox_arc.git``` |

2. Build container image using the following command:

    ```console
    # cd dockervobox
    # docker build -t voboximg .
    ```
    Or pull a pre-built container from our registry:
    ```console
    # docker pull gitlab-registry.cern.ch/jalien/dockervobox/voboximg:latest
    # docker tag gitlab-registry.cern.ch/jalien/dockervobox/voboximg:latest voboximg:latest
    ```

    !!! warning
        Be sure to change the default root password! This is simply __root__ in the above image.

3. Create and launch a new CentOS container connected to the MACVLAN bridge:

    ```console
    # docker run -d -v /cvmfs:/cvmfs \
        -h myalienvo.cern.ch \
        --name=myvocontainer \
        --net=docknet \
        --ip=137.138.47.251 \
        --ip6=2001:1458:201:b50e::100:3e \
        voboximg
    ```

    ??? note "Command details"
        __-v__ mounts the directory __/cvmfs__ from the host as __/cvmfs__ within the container<br>
        __-h__ sets the hostname __myalienvo.cern.ch__<br>
        __--name__ sets the container name (as seen from Docker) to __myvocontainer__<br>
        __--net__ attaches the container to the previously created MACVLAN bridge __docknet__<br>
        __--ip/ip6__ sets the desired network IPs for the container

    !!! info
        Docker gives containers limited access to the host kernel, which __will prevent some tools from working properly__.
        A possible workaround is to give the container additional privileges (see [advanced topics](#privileged-mode) for details).<br><br>
        As opposed to mounting CVMFS from the host, it is also possible to have it installed normally within the container.
        However, this will only work on a container with full access privileges.
        Be aware that unless otherwise configured, this will give each container its own CVMFS cache.

## Service Management

Almost all services are started automatically alongside the container.
If needed, you can manage most of them manually by standard Linux ```systemctl``` commands.
The exception is the proxy renewal daemon, which is managed as follows:

```console
# /services/alice-box-proxyrenewal {start|stop|restart|status|proxy}
```

!!! example "Example Deployment"
    1. Install a host certificate.
    2. Start the services using the following commands:
      ```console
      # /services/alice-box-proxyrenewal start < /dev/null
      # /usr/sbin/edg-mkgridmap --output=/etc/grid-security/grid-mapfile --safe
      ```
    3. Adjust ```~/.alien/Environment``` as needed (not for JAliEn).


## Useful Commands

The following table lists commands that may be of use when working with the VoBox containers.<br> 
Refer to [command line Docker documentation](https://docs.docker.com/engine/reference/commandline/cli/) for more details.

| Command | Description |
|:--------|:------------|
| ```# docker start myvocontainer``` | Start the container<br>Note: This way, the container will remain active until you type<br> ```# docker stop myvocontainer``` |
| ```# docker exec -it myvocontainer /bin/bash``` | Start a bash session within the container |
| ```$ ssh root@your.ip.vobox.here``` | Login into the container |
| ```# docker ps``` | List active containers |
| ```# docker ps -a``` | List all containers |
| ```# docker images``` | List all images cached locally |
| ```# docker commit myvocontainer myvoimagename``` | Save container state as an image.<br> With __myimagename__ being the desired image name |
| ```# docker export mycontainer > /home/myexportcontainer.tar``` | Save the container as a tar file.<br>With __/home/myexportedcontainer.tar__ being the directory and name for the exported container |

??? help "Docker commit vs export"
    The ```commit``` command will save the container as an image in Docker's internal format.
    It will also preserve the container's history, layers and metadata, and also create a new layer on top.
    Beware that an image will rapidly grow in size if it is continuously being committed, as a result of all the stacked layers.<br><br>
    The ```export``` command will save the container to a tar file, that can be reimported and used to launch new containers on the same machine, or uploaded and/or transferred elsewhere.
    However, it does not store any history or metadata, and will delete all previous layers except the current.
    Exporting and reimporting an image can thus be used to __flatten__ a container that has gained a large file size, e.g. as a result of having previously used ```commit``` several times.


## Advanced Topics

### Privileged Mode

For the sake of isolation, access to the kernel is restricted within the containers. 
To enable some access for networking tools, such as iptables, add ```--cap-add=NET_ADMIN``` when [creating the container](#create-container). More access can be given through ```--cap-add=SYS_ADMIN```, and full access can be given with ```--privileged```. 

!!! warning
    Be aware that this will create pathways for potentially breaking container isolation.

### Open Files Limit

If you plan on running more than one VOBOX container on a single host, the system limit for the  maximum number of open file descriptors will need to be increased.
The default ```ulimit``` is otherwise bound to be reached at some point, causing the containers and their processes to become irresponsive and terminate.

### Access from Host

As mentioned earlier, the host will be unable to reach/ping the containers, and vice versa. A workaround is to create an additional ("normal") Docker bridge, and have the host / containers connect to it. 

### AutoFS Bug

CVMFS may respond with a <span style="color: red">"too many symbolic links"</span> error when accessed from a new container or after a reboot.
This is known as an _autofs bug_, and can be avoided by disabling autofs on the host and mounting your CVMFS directory manually. 
If you prefer to leave autofs enabled, you can remove the error by accessing a directory within CVMFS on the host (e.g ```/cvmfs/alice.cern.ch```), and then restarting the container. 

!!! warning
    Be aware that this error will likely return on the next reboot if autofs is left enabled.

