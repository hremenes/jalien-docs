### **Direct Access split strategy**

Split all datainput files evenly into different subjobs. This will force downloads of files to sites unless
this is taken into account and mitigated by having all files on same site and set closeSE requirement.

Example JDL:
``` 
User = "jalien";
JobTag = {
"Direct Access split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputDataCollection = "LF:/alice/cern.ch/user/j/jalien/inputdatafileCollection.xml,nodownload";
Split = "directaccess";
SplitMaxInputFileNumber = "10";
```
Mandatory flags in JDL:  
[SplitMaxInputFileNumber](../../../jdl_syntax#splitmaxinputfilenumber)  
or  
[SplitMaxInputFileSize](../../../jdl_syntax#splitmaxinputfilesize)  

Optional flags in JDL:  
[OrderLFN](../../../jdl_syntax#orderlfn)  

