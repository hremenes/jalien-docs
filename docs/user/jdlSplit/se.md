### **Storage Element split strategy**

Seperates inputdata files into different subjobs based on physical Storage Elements location files are stored on
and file size or number of files restrictions. There is also a default minimum value for subjob size, where inputdata files
are merged to form a larger subjob if it is too small, this minimum subjobs size value can be set by user. Requirements 
for matching with sites that locally have inputdata files are also added. Maximum number of files per subjob or size 
per subjobs MUST be set.

Example JDL:
``` 
User = "jalien";
JobTag = {
"Storage Element split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputDataCollection = "LF:/alice/cern.ch/user/j/jalien/inputdatafileCollection.xml,nodownload";
Split = "se";
SplitMaxInputFileNumber = "10";
```
Mandatory flags in JDL  
[SplitMaxInputFileNumber](../../../jdl_syntax#splitmaxinputfilenumber)  
or  
[SplitMaxInputFileSize](../../../jdl_syntax#splitmaxinputfilesize)  

Optional flags in JDL:  
[SplitMinInputFileNumber](../../../jdl_syntax#splitmininputfilenumber)  
[OrderLFN](../../../jdl_syntax#orderlfn)  
