### **File split strategy**

Divides inputdata files based on full LFN path, resulting in one file per subjob as LFN's are unique. Will also add 
Close.SE requirements to the LFN to match with site that have local access to the inputdata file.

Example JDL:
``` 
User = "jalien";
JobTag = {
"File split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputData = {
"LF:/alice/cern.ch/user/j/jalien/jobs/inputdatafile"
};
Split = "file";
```