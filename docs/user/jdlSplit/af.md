### **Analysis Facility split strategy**

Analysis Facility split meant for cases where files all share a Storage Element and forcing jobs to run on that site.
As of right now uses only closeSE in requirement to get SE.

Example: /alice/cern.ch/user/j/jalien/inputdatafile --> /alice/cern.ch/user/j/jalien

Example JDL:
``` 
User = "jalien";
JobTag = {
"Directory split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputDataCollection = "LF:/alice/cern.ch/user/j/jalien/inputdatafileCollection.xml,nodownload";
Split = "directory";
```
Mandatory flags in JDL:  
[SplitMaxInputFileNumber](../../../jdl_syntax#splitmaxinputfilenumber)  
or  
[SplitMaxInputFileSize](../../../jdl_syntax#splitmaxinputfilesize)

Optional flags in JDL:  
[ForceOnlySEInput](../../../jdl_syntax#forceonlyseinput)  
[MaxInputMissingThreshold](../../../jdl_syntax#maxinputmissingthreshold)  
[OrderLFN](../../../jdl_syntax#orderlfn)
