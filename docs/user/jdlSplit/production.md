### **Production split strategy**

Duplicate the job a number of time equal to an interval defined by an end and a start number 
Such as: production:[Start]-[End].
This will also set the counter for [#alien_counter#](../../../jdl_syntax#counter) which is useful for
Monte Carlo Simulations.

Example JDL:
``` 
User = "jalien";
JobTag = {
"Production split!"
};
Packages = {
"VO_ALICE@O2Physics::daily-20241202-0000-1"
};
Executable = "/alice/cern.ch/user/j/jalien/bin/splitting.sh";
InputDataCollection = "LF:/alice/cern.ch/user/j/jalien/inputdatafileCollection.xml,nodownload";
Split = "production:10-1";
```

