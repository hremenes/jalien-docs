# Troubleshooting

## How to get Help

If you are still experiencing problems or have questions, please let us know through any of the following channels:

| Option | Link |
|-|-|
| Ask us on the JAliEn channel | [Mattermost channel](https://mattermost.web.cern.ch/alice/channels/jalien) |
| Email to ALICE Analysis Task Force list | [alice-project-analysis-task-force@cern.ch](mailto:alice-project-analysis-task-force@cern.ch) |
| Check out or create a JIRA ticket |  [JAliEn JIRA](https://alice.its.cern.ch/jira/projects/JAL/) |
| Contact the developers mailing list | [jalien-support@cern.ch](mailto:jalien-support@cern.ch) |
| Pass by our offices | [building 12, 1st floor](https://maps.web.cern.ch/?n=[%2712%27]) |

## Contact Us

(sorted by the preferred load balancing)

| Name                 | E-mail                                                                      |
|----------------------|-----------------------------------------------------------------------------|
| Volodymyr  Yurchenko | [volodymyr.yurchenko@cern.ch](mailto:volodymyr.yurchenko@cern.ch)           |
| Adrian     Sevcenco  | adrianDOTsevcencoATNOSPAMcernDOTch                                          |
| Maksim     Melnik    | [maksim.melnik.storetvedt@cern.ch](mailto:maksim.melnik.storetvedt@cern.ch) |
| Costin     Grigoras  | [costin.grigoras@cern.ch](mailto:costin.grigoras@cern.ch)                   |
| Latchezar  Betev     | [latchezar.betev@cern.ch](mailto:latchezar.betev@cern.ch)                   |
