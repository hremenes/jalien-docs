# alien.py Command reference guide

### SEqos
```
Command format: SEqos <SE name>
Return the QOS tags for the specified SE (ALICE:: can be ommited and capitalization does not matter)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### access
```
usage: access   [options] <read|write> <lfn> [<specs>]
    -s     :  for write requests, size of the file to be uploaded, when known
    -m     :  for write requests, MD5 checksum of the file to be uploaded, when known
    -j     :  for write requests, the job ID that created these files, when applicable
    -f     :  for read requests, filter the SEs based on the given specs list
    -u     :  for read requests, print http(s) URLs where available, and the envelopes in urlencoded format
```

---

### archiveList
```
usage: archiveList   <filename1> [<or uuid>] ...
```

---

### cat
```
cat <LFN>
Download specified LFN as temporary and pass it to system cat command.
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### ccdb
```
ccdb [-host FQDN] [-history] [-nicetime] QUERY
where query has the form of:
task name / detector name / start time [ / UUID]
or
task name / detector name / [ / time [ / key = value]* ]

-run RUN_NR      : filter for a specific run (converts to /runNumber=RUN_NR})
-host            : specify other ccdb server than alice-ccdb.cern.ch
-history         : use browse to list the whole history of the object
-limit N         : limit the history to latest N elements; default = 10
-unixtime        : print the unixtime instead of human time
-header "STRING" : add header declarations/filters to the requests sent to CCDB server
-get             : download the specified object/objects - full path will be kept
-dst DST_DIR     : set a specific destination for download
-mirror          : create CCDB snapshot as per specifications of CCDB server

```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### cd
```
usage: cd   [dir]
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### cert-info
```
Print user certificate information
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### cert-verify
```
Verify the user cert against the found CA stores (file or directory)
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### certkey-match
```
Check match of user cert with key cert
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### changeDiff
```
Show changes between the current version of the file and the previous one (same file name with a '~' suffix)
usage: changeDiff   [<filename>]

options:
```

---

### checkAddr
```
checkAddr [reference] fqdn/ip port
defaults are: alice-jcentral.cern.ch 8097
reference arg will check connection to google dns and www.cern.ch
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### chmod
```
Usage: chmod -R <octal mode> <path> [<path>...]

Changes the access mode for a catalogue path
-R : do a recursive chmod starting from the given path
```

---

### chown
```
Usage: chown -R <user>[.<group>] <file>

Changes an owner or a group for a file
-R : do a recursive chown
```

---

### commandlist
```
usage: commandlist
```

---

### commit
```
usage: commit   API command only, you should not need to call this directly
```

---

### cp
```
Command format is of the form of (with the strict order of arguments):
        cp <options> src dst
        or
        cp <options> -input input_file
        or
        cp <options> -dst dest_dir file1 file2 ... fileN
where src|dst are local files if prefixed with file:// or file: or grid files otherwise
and -input argument is a file with >src dst< pairs
after each src,dst can be added comma separated specifiers list in the form of: @<QOS>:N,SE1,SE2,!SE3
QOS is a tag of the storage element (usually "disk") followed by the N requested replicas
Additional replicas can be requested by the name of storage elements. (additional to QOS specifications),
or exclusion of storages (prefixing with exclamation mark).
%ALIEN alias have the special meaning of AliEn user home directory
options are the following :
-h : print help
-dryrun  : just print the src,dst pairs that would have been transfered without actually doing so
-f       : No longer used flag! md5 verification of already present destination is default; disable with -fastcheck
-fastcheck               : When already present destination is check for validity, check only size not also md5
-S <aditional streams>   : uses num additional parallel streams to do the transfer. (max = 15)
-chunks <nr chunks>      : number of chunks that should be requested in parallel
-chunksz <bytes>         : chunk size (bytes)
-T <nr_copy_jobs>        : number of parralel copy jobs from a set (for recursive copy); defaults to 8 for downloads
-timeout <seconds>       : the job will fail if did not finish in this nr of seconds
-retry <times>           : retry N times the copy process if failed
-ratethreshold <bytes/s> : fail the job if the speed is lower than specified bytes/s
-noxrdzip: circumvent the XRootD mechanism of zip member copy and download the archive and locally extract the intended member.
N.B.!!! for recursive copy (all files) the same archive will be downloaded for each member.
If there are problems with native XRootD zip mechanism, download only the zip archive and locally extract the contents

For the recursive copy of directories the following options (of the find command) can be used:
-j jobid           : select only the files created by the job with jobid (for recursive copy)
-l int             : copy only <count> nr of files (for recursive copy)
-o int             : skip first <offset> files found in the src directory (for recursive copy)
-e exclude_pattern : exclude files that match this pattern (for recursive copy)

Further filtering of the files can be applied with the following options:
-glob    <globbing pattern> : this is the usual AliEn globbing format; N.B. this is NOT a REGEX!!! defaults to all "*"
-select  <pattern>          : select only these files to be copied; N.B. this is a REGEX applied to full path!!!
-name    <pattern>          : select only these files to be copied; N.B. this is a REGEX applied to a directory or file name!!!
-name    <verb>_string      : where verb = begin|contain|ends|ext and string is the text selection criteria.
verbs are aditive  e.g. -name begin_myf_contain_run1_ends_bla_ext_root
N.B. the text to be filtered cannont have underline i.e >_< within!!!

-exclude     string            : (client-side) exclude result containing this string
-exclude_re  pattern           : (client-side) exclude result matching this regex
-user        string            : (client-side) match the user
-group       string            : (client-side) match the group
-jobid       string            : (client-side) match the jobid
-minsize   / -maxsize    int   : (client-side) restrict results to min/max bytes (inclusive)
-mindepth  / -maxdepth   int   : (client-side) restrict results to min/max depth
-min-ctime / -max-ctime  int(unix time) : (client-side) restrict results age to min/max unix-time

```

!!! warning "client-side implementation or improvement of the central servers version"

---

### deleteMirror
```
Removes a replica of a file from the catalogue
Usage:
        deleteMirror [-g] <lfn> <se> [<pfn>]

Options:
   -g: the lfn is a guid
```

---

### df
```
Shows free disk space
Usage: df
```

---

### dirs
```
Directory stacking implementation (as in Linux shell):
dirs [-clpv] [+N | -N]
popd [-n] [+N | -N]
pushd [-n] [+N | -N | dir]
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### du
```
Gives the disk space usage of one or more directories
usage: du   [-ncs] <path>

options:
    -n     :  Print raw numbers in machine readable format
    -c     :  Include collections in the summary information
    -s     :  Print a summary of all parameters
```

---

### echo
```
usage: echo   [text]
```

---

### edit
```
Command format: edit lfn
After editor termination the file will be uploaded if md5 differs
-datebck : the backup filename will be date based
N.B. EDITOR env var must be set or fallback will be mcedit (not checking if exists)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### exec
```
Command format: exec lfn list_of_arguments
N.B.! The output and error streams will be captured and printed at the end of execution!
for working within application use <edit>
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### exit
```
Command format: exit [code] [stderr|err] [message]
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### exitcode | $?
```
return last command exitcode
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### find
```
usage: find   [flags] <path> <pattern>


options:
    -a     :  show hidden .* files
    -s     :  no sorting
    -c     :  print the number of matching files
    -x <target LFN>        :  create the indicated XML collection with the results of the find operation. Use '-' for screen output of the XML content.
    -d     :  return also the directories
    -w[h]  :  long format, optionally human readable file sizes
    -j <queueid>           :  filter files created by a certain job ID
    -l <count>             :  limit the number of returned entries to at most the indicated value
    -o <offset>            :  skip over the first /offset/ results
    -r     :  pattern is a regular expression
    -f     :  return all LFN data as JSON fields (API flag only)
    -y     :  (FOR THE OCDB) return only the biggest version of each file
    -S <site name>         :  Sort the returned list by the distance to the given site
    -e <pattern>           :  Exclude pattern
```

---

### find2
```
Client-side implementation of find, it contain the following helpers.
Command formant: find2 <options> <directory>
N.B. directory to be search for must be last element of command
-glob <globbing pattern> : this is the usual AliEn globbing format; N.B. this is NOT a REGEX!!! defaults to all "*"
-select <pattern>        : select only these files to be copied; N.B. this is a REGEX applied to full path!!!
-name <pattern>          : select only these files to be copied; N.B. this is a REGEX applied to a directory or file name!!!
-name <verb>_string      : where verb = begin|contain|ends|ext and string is the text selection criteria.
verbs are aditive  e.g. -name begin_myf_contain_run1_ends_bla_ext_root
N.B. the text to be filtered cannont have underline i.e >_< within!!!

-exclude     string            : (client-side) exclude result containing this string
-exclude_re  pattern           : (client-side) exclude result matching this regex
-user    string: (client-side) match the user
-group   string: (client-side) match the group
-jobid   string: (client-side) match the jobid
-minsize   / -maxsize    int   : (client-side) restrict results to min/max bytes (inclusive)
-mindepth  / -maxdepth   int   : (client-side) restrict results to min/max depth
-min-ctime / -max-ctime  int(unix time) : (client-side) restrict results age to min/max unix-time

The server options:
usage: find   [flags] <path> <pattern>


options:
    -a     :  show hidden .* files
    -s     :  no sorting
    -c     :  print the number of matching files
    -x <target LFN>        :  create the indicated XML collection with the results of the find operation. Use '-' for screen output of the XML content.
    -d     :  return also the directories
    -w[h]  :  long format, optionally human readable file sizes
    -j <queueid>           :  filter files created by a certain job ID
    -l <count>             :  limit the number of returned entries to at most the indicated value
    -o <offset>            :  skip over the first /offset/ results
    -r     :  pattern is a regular expression
    -f     :  return all LFN data as JSON fields (API flag only)
    -y     :  (FOR THE OCDB) return only the biggest version of each file
    -S <site name>         :  Sort the returned list by the distance to the given site
    -e <pattern>           :  Exclude pattern
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### fquota
```
fquota: Displays information about File Quotas.
usage: fquota   list [-<options>]
Options:
  -unit = B|K|M|G: unit of file size
```

---

### getCAcerts
```
Download CA certificates from ALICE alien-cas repository in ~/.globus/certificates
-h/-help : this help information
1st argument string will be taken as destination for certificates directory
to use them preferentially do:
export X509_CERT_DIR=$HOME/.globus/certificates
or just
export ALIENPY_USE_LOCAL_CAS=1
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### getCE
```
Command format: getCE [-name string] [-host string] [-part string]>
Return the informations for the selection
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### getSE
```
Command format: getSE <-id | -name | -srv> identifier_string
Return the specified property for the SE specified label
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### grep
```
usage: grep   [-linux grep options] <pattern> [<filename>]+

options:
```

---

### groups
```
groups [<username>]
shows the groups current user is a member of.
```

---

### guid2lfn
```
usage: guid2lfn   <GUID>
```

---

### guidinfo
```
usage: guidinfo   <uuid> ...
```

---

### help | ?
```
Project documentation can be found at:
https://jalien.docs.cern.ch/
https://gitlab.cern.ch/jalien/xjalienfs/blob/master/README.md
the following commands are available:
$?      ?       SEqos   access  archiveList             
cat     ccdb    cd      cert-info               cert-verify             
certkey-match           changeDiff              checkAddr               chmod   chown   
commandlist             commit  cp      deleteMirror            df      
dirs    du      echo    edit    exec    
exit    exitcodefind    find2   fquota  
getCAcerts              getCE   getSE   grep    groups  
guid2lfnguidinfohelp    home    jobInfo 
jobListMatch            jquota  kill    la      less    
lfn2guidlfn2uri lfnexpiretime           listCEs listFilesFromCollection 
listSEDistance          listSEs listTransfer            listpartitions          ll      
lla     logout  ls      masterjob               mcedit  
md5sum  mirror  mkdir   more    motd    
mv      nano    optimiserLogs           packagespfn     
pfn-status              ping    popd    prompt  ps      
pushd   pwd     queryML quit    quota   
registerOutput          resubmitresyncLDAP              rm      rmdir   
run     setCEstatus             setSite showTagValue            siteJobs
stat    submit  testSE  time    timing  
toXml   token   token-destroy           token-info              token-init              
token-verify            tokenkey-match          top     touch   type    
uptime  user    uuid    version vi      
vim     w       whereis whoami  whois   
xrd_config              xrd_pingxrd_stats               xrdstat 
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### home
```
Return user home
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### jobInfo
```
Command format: jobInfo id1,id2,.. [ -trace ] [ -proc ]
        Print job information for specified ID(s)
        -trace will show the trace messages
        -proc will show the proc messages
        
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### jobListMatch
```
jobListMatch: print all the CEs that can run a certain job
usage: jobListMatch    [jobId]

options:
```

---

### jquota
```
usage: jquota   Displays information about Job Quotas.

options:
    list [username]*       :  get job quota information for the current account, or the indicated ones
    set <user> <field> <value>  :  to set quota fileds (one of  maxUnfinishedJobs, maxTotalCpuCost, maxTotalRunningTime)
```

---

### kill
```
usage: kill   <jobId> [<jobId>[,<jobId>]]
```

---

### less
```
less <LFN>
Download specified LFN as temporary and pass it to system less command.
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### lfn2guid
```
usage: lfn2guid   <filename>
```

---

### lfn2uri
```
Command format : lfn2uri <lfn> <local_file?> [meta] [write|upload] [strict] [http]
It will print the URIs for lfn replicas
local_file : required only for write|upload URIs
meta : will write in current directory the metafile and will return the string to be used with xrdcp
write|upload : request tokens for writing/upload; incompatible with <meta> argument
strict : lfn specifications will be considered to be strict
http : URIs will be for http end-points of enabled SEs

```

!!! warning "client-side implementation or improvement of the central servers version"

---

### lfnexpiretime
```
usage: lfnexpiretime   [-options] [<file>]

options:
    -r     :  removes the expire time set for an LFN
    -a     :  add a new expire time for the given LFN
    -e     :  extends the current expire time for the given LFN
    -d <number>            :  specifies the number of days in the expire time
    -w <number>            :  specifies the number of weeks in the expire time
    -m <number>            :  specifies the number of months in the expire time
    -y <number>            :  specifies the number of years in the expire time
```

---

### listCEs
```
listCEs: print all (or a subset) of the defined CEs with their details
usage: listCEs    [-s] [CE name] [CE name] ...

options:
    -s     :  print summary information
    -p     :  filter by partition names
    -v     :  print verbose output (including partitions)
```

---

### listFilesFromCollection
```
usage: listFilesFromCollection   [-options] collection

options:
    -z     :  show size and other file details
    -s     :  silent (API only)
```

---

### listSEDistance
```
listSEDistance: Returns the closest working SE for a particular site. Usage

options:
    -site  :  site to base the results to, instead of using the default mapping of this client to a site
    -read  :  use the read metrics, optionally with an LFN for which to sort the replicas. Default is to print the write metrics.
    -qos   :  restrict the returned SEs to this particular tag
```

---

### listSEs
```
listSEs: print all (or a subset) of the defined SEs with their details
usage: listSEs   [-qos filter,by,qos] [-s] [SE name] [SE name] ...

options:
    -qos   :  filter the SEs by the given QoS classes. Comma separate entries for 'AND', pass multiple -qos options for an 'OR'
    -s     :  print summary information
```

---

### listTransfer
```
listTransfer: returns all the transfers that are waiting in the system
        Usage:
listTransfer [-status <status>] [-user <user>] [-id <queueId>] [-verbose] [-master] [-summary] [-all_status] [-jdl] [-destination <site>]  [-list=<number(all transfers by default)>] [-desc]
```

---

### listpartitions
```
listpartitions: print all (or a subset) of the defined partitions
usage: listpartitions    [-v] [Partition name] [Partition name] ...

options:
    -v     :  print verbose output (including member CEs)
```

---

### logout
```
Command format: exit [code] [stderr|err] [message]
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### ls | la | ll | lla
```
usage: ls   [-options] [<directory>]

options:
    -l     :  long format
    -a     :  show hidden .* files
    -F     :  add trailing / to directory names
    -b     :  print in guid format
    -c     :  print canonical paths
    -H     :  human readable file sizes (1024-based); implies '-l'
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### masterjob
```
usage: masterjob   <jobIDs> [-options]

options:
    -status <status>       :  display only the subjobs with that status
    -id <id>               :  display only the subjobs with that id
    -site <id>             :  display only the subjobs on that site
    -printid               :  print also the id of all the subjobs
    -printsite             :  split the number of jobs according to the execution site
```

---

### mcedit
```
Command format: edit lfn
After editor termination the file will be uploaded if md5 differs
-datebck : the backup filename will be date based
N.B. EDITOR env var must be set or fallback will be mcedit (not checking if exists)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### md5sum
```
usage: md5sum   <filename1> [<or guid>] ...
```

---

### mirror
```
mirror Copies/moves a file to one or more other SEs
 Usage:
	mirror [-g] [-try <number>] [-r SE] [-S [se[,se2[,!se3[,qos:count]]]]] <lfn> [<SE>]
 -g:     Use the lfn as a guid
 -S:     specifies the destination SEs/tags to be used
 -r:     remove this source replica after a successful transfer (a `move` operation)
 -try <attempts>     Specifies the number of attempts to try and mirror the file (default 5)
```

---

### mkdir
```
usage: mkdir   [-options] <directory> [<directory>[,<directory>]]

options:
    -p     :  create parents as needed
    -silent:  execute command silently
```

---

### more
```
more <LFN>
Download specified LFN as temporary and pass it to system more command.
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### motd
```
usage: motd   Message of the day
```

---

### mv
```
usage: mv    <LFN>  <newLFN>
```

---

### nano
```
Command format: edit lfn
After editor termination the file will be uploaded if md5 differs
-datebck : the backup filename will be date based
N.B. EDITOR env var must be set or fallback will be mcedit (not checking if exists)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### optimiserLogs
```
Usage: lastOptimiserLog [-l] [-v] [-f <frequency>] <classnames or metanames>

    Gets the last log from the optimiser
    -v : Verbose, displays the frequency, last run timestamp and log
    -l : List the class names that match a query
    -f <value> : Frequency, in seconds
```

---

### packages
```
usage: packages     list available packages
    -platform              :  Platform name, default Linux-x86_64
    -all   :  List packages on all platforms. Equivalent to '-p all'
```

---

### pfn
```
Command format : pfn [lfn]
It will print only the list of associated pfns (simplified form of whereis)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### pfn-status
```
Command format: pfn_status <pfn>|<lfn>
It will return all flags reported by the xrootd server - this is direct access to server
pfn is identified by prefix root://; if missing the argument will be taken to be a lfn
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### ping
```
ping <count>
where count is integer
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### popd
```
Directory stacking implementation (as in Linux shell):
dirs [-clpv] [+N | -N]
popd [-n] [+N | -N]
pushd [-n] [+N | -N | dir]
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### prompt
```
Toggle the following in the command prompt : <date> for date information and <pwd> for local directory
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### ps
```
usage: ps   [-options]

options:
    -F l | -Fl | -L        :  long output format
    -f <flags|status>      :  any number of (long or short) upper case job states, or 'a' for all, 'r' for running states, 'f' for failed, 'd' for done, 's' for queued
    -u <userlist>        
    -s <sitelist>        
    -n <nodelist>        
    -m <masterjoblist>   
    -o <sortkey>         
    -j <jobidlist>       
    -l <query-limit>     

    -M     :  show only masterjobs
    -X     :  active jobs in extended format
    -A     :  select all owned jobs of you
    -W     :  select all jobs which are waiting for execution of you
    -E     :  select all jobs which are in error state of you
    -a     :  select jobs of all users
    -b     :  do only black-white output
    -jdl <jobid>           :  display the job jdl
    -trace <jobid>         :  display the job trace information
    -id    :  only list the matching job IDs, for batch processing (implies -b)
    -q     :  quiet, no user printable output, API function only
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### pushd
```
Directory stacking implementation (as in Linux shell):
dirs [-clpv] [+N | -N]
popd [-n] [+N | -N]
pushd [-n] [+N | -N | dir]
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### pwd
```
pwd : print/return the current work directory
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### queryML
```
usage: queryML <ML node>
time range can be specified for a parameter:
/[starting time spec]/[ending time spec]/parameter
where the two time specs can be given in absolute epoch timestamp (in milliseconds), as positive values,
or relative timestamp to `now`, when they are negative.
For example `-60000` would be "1 minute ago" and effectively `-1` means "now".
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### quit
```
Command format: exit [code] [stderr|err] [message]
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### quota
```
Client-side implementation that make use of server's jquota and fquota (hidden by this implementation)
Command format: quota [user]
if [user] is not provided, it will be assumed the current user
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### registerOutput
```
usage: registerOutput   <job ID>

options:
```

---

### resubmit
```
resubmit: resubmits a job or a group of jobs by IDs
        Usage:
resubmit <jobid1> [<jobid2>....]
```

---

### resyncLDAP
```
Usage: resyncLDAP

    Synchronizes the DB with the updated values in LDAP
```

---

### rm
```
usage: rm    <LFN> [<LFN>[,<LFN>]]

options:
    -f     :  ignore nonexistent files, never prompt
    -r, -R :  remove directories and their contents recursively
    -i     :  prompt before every removal (for JSh clients)
```

---

### rmdir
```
usage: rmdir    [<option>] <directory>

options:
    --ignore-fail-on-non-empty  :    ignore each failure that is solely because a directory is non-empty
    -p     :  --parents   Remove DIRECTORY and its ancestors.  E.g., 'rmdir -p a/b/c' is similar to 'rmdir a/b/c a/b a'.
    -v     :  --verbose  output a diagnostic for every directory processed
           :  --help      display this help and exit
           :  --version  output version information and exit
    -silent:  execute command silently
```

---

### run
```
Command format: run shell_command arguments lfn
the lfn must be the last element of the command
N.B.! The output and error streams will be captured and printed at the end of execution!
for working within application use <edit>
additiona arguments recognized independent of the shell command:
-force : will re-download the lfn even if already present
-noout : will not capture output, the actual application can be used
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### setSite
```
usage: setSite   [site name]

options:
```

---

### showTagValue
```
usage: showtagValue   [flags] <filename> [<filename>...]

options:
    -t     :  restrict to this (comma separated) tag list only (default is to return all available tags)
    -c     :  restrict to these (comma separated) list of attributes
    -l     :  list available tags only
```

---

### siteJobs
```
Command format: siteJobs <SITE ID> [ -id ] [ -running ] [ -status string] [ -user string ]
        Print jobs id or information for jobs associated with a site; use getCE command to identify site names
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### stat
```
usage: stat   [-v] <filename1> [<or uuid>] ...
    -v     :  More details on the status.
```

---

### submit
```
usage: submit   <URL>

    <URL> => <LFN>
    <URL> => file:///<local path>
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### testSE
```
Test the functional status of Grid storage elements
Usage: testSE [options] <some SE names, numbers or @tags>
    -v     :  verbose error messages even when the operation is expected to fail
    -c     :  show full command line for each test
    -t     :  time each operation
    -a     :  test all SEs (obviously a very long operation)
```

---

### time
```
Usage: time <times>  <command> [command_arguments]
```

---

### timing
```
usage: timing   [on|off]

options:
    return server side timing information
```

---

### toXml
```
usage: toXml   [-i] [-x xml_file_name] [-a] [-l list_from] [lfns]

options:
    -i     :  ignore missing entries, continue even if some paths are not/no longer available
    -x     :  write the XML content directly in this target AliEn file
    -a     :  (requires -x) append to the respective collection
    -l     :  read the list of LFNs from this file, one path per line
Additionally the client implements these options:
-local: specify that the target lfns are local files
for -x (output file) and -l (file with lfns) the file: and alien: represent the location of file
the inferred defaults are that the target files and the output files are of the same type
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### token
```
Print only command!!! Use >token-init< for token (re)generation, see below the arguments
usage: token   [-options]

options:
    -u <username>          :  switch to another role of yours
    -v <validity (days)>   :  default depends on token type
    -t <tokentype>         :  can be one of: job, jobagent, host, user (default)
    -jobid <job DN extension>  :  expected to be present in a job token
    -hostname <FQDN>       :  required for a host certificate
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### token-destroy
```
Delete the token{cert,key}.pem files
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### token-info
```
Print token certificate information
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### token-init
```
INFO: token is automatically created, use this for token customization
usage: token-init   [-options]

options:
    -u <username>          :  switch to another role of yours
    -v <validity (days)>   :  default depends on token type
    -t <tokentype>         :  can be one of: job, jobagent, host, user (default)
    -jobid <job DN extension>  :  expected to be present in a job token
    -hostname <FQDN>       :  required for a host certificate
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### token-verify
```
Print token certificate information
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### tokenkey-match
```
Check match of user token with key token
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### top
```
usage: top
```

---

### touch
```
usage: touch    <LFN> [<LFN>[,<LFN>]]
```

---

### type
```
usage: type   <lfn> 
    Print the LFN type (file / directory / collection)
```

---

### uptime
```
usage: uptime       

options:
```

---

### user
```
usage: user   <user name>

    Change effective role as specified.
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### uuid
```
usage: uuid   <uuid|filename> [<uuid|filename> ...]

    Decode v1 UUIDs and display the interesting bits
```

---

### version
```
alien.py version: 1.5.2
alien.py version date: 20231012_203723
alien.py version hash: 1b66960
alien.py location: /home/adrian/work-ALICE/jalien_py/alienpy/alien.py
script location: 
Interpreter: /usr/bin/python3.11
Python version: 3.11.5 (main, Aug 28 2023, 00:00:00) [GCC 13.2.1 20230728 (Red Hat 13.2.1-1)]
XRootD version: 5.5.3
XRootD path: /home/adrian/.local/lib/python3.11/site-packages/XRootD/client/__init__.py
```

!!! warning "client-side implementation"

!!! note "No connection to central servers needed"

---

### vi
```
Command format: edit lfn
After editor termination the file will be uploaded if md5 differs
-datebck : the backup filename will be date based
N.B. EDITOR env var must be set or fallback will be mcedit (not checking if exists)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### vim
```
Command format: edit lfn
After editor termination the file will be uploaded if md5 differs
-datebck : the backup filename will be date based
N.B. EDITOR env var must be set or fallback will be mcedit (not checking if exists)
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### w
```
usage: w   Show currently active users on the Grid

options:
    -a     :  Sort by the number of active jobs
    -w     :  Sort by the number of waiting jobs
    -r     :  Reverse sorting order
```

---

### whereis
```
usage: whereis   [-options] [<filename>]

options:
    -g     :  use the lfn as guid
    -r     :  resolve links (do not give back pointers to zip archives)
    -l     :  lookup the LFN of the ZIP archive (slow and expensive IO operation, only use it sparingly!)
```

---

### whoami
```
usage: whoami       

options:
    -v     :  verbose details of the current identity
```

---

### whois
```
usage: whois   [account name]

options:
    -s     :  search for the given string(s) in usernames
    -f     :  also search in full names
    -e     :  search in email addresses too
    -d     :  search in X509 DN (subject) fields
    -a     :  search for the given string in all the above fields
```

---

### xrd_config
```
Command format: xrd_config [-v | -verbose] fqdn[:port] | SE name | SE id
It will use the XRootD query config to get the current server properties
verbose mode will print more about the server configuration
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### xrd_ping
```
Command format: xrd_ping [-c count] fqdn[:port] | SE name | SE id
It will use the XRootD connect/ping option to connect and return a RTT
```

!!! warning "client-side implementation or improvement of the central servers version"

---

### xrd_stats
```
Command format: xrd_stats [ -xml | -xmlraw | -compact  ]  fqdn[:port] | SE name | SE id
It will use the XRootD query stats option to get the server metrics
-xml : print xml output (native to xrootd)
-xmlraw : print rawxml output without any indentation
-compact : print the most compact version of the output, with minimal white space

```

!!! warning "client-side implementation or improvement of the central servers version"

---

### xrdstat
```
usage: xrdstat   [-d [-i]] [-v] [-p PID,PID,...] [-s SE1,SE2,...] [-c] <filename1> [<or UUID>] ...

options:
    -d     :  Check by physically downloading each replica and checking its content. Without this a stat (metadata) check is done only.
    -i     :  When downloading each replica, ignore `stat` calls and directly try to fetch the content.
    -s     :  Comma-separated list of SE names to restrict the checking to. Default is to check all replicas.
    -c     :  Print the full command line in case of errors.
    -v     :  More details on the status.
    -p     :  Comma-separated list of job IDs to check the input data of
    -o     :  Only show the online status (for files with tape replicas in particular)
    -O     :  Request the file to be brought online
    -4     :  Force IPv4 usage on all commands
    -6     :  Force IPv6 usage on all commands
```

---

