# QuickStart

How to start using JAliEn clients today

  - From CVMFS: load a AliPhysics version that has the _\_JALIEN_ suffix or just the shell with `alienv enter xjalienfs`
  - Local software builds (before alidist update): `aliBuild build AliPhysics --defaults jalien` 
  - Local software builds (after alidist update): any ROOT6 build comes with JAliEn support 
  - To get access to JShell and JBox: `aliBuild build JAliEn --defaults jalien` 
  - To get _alien.py_ shell only: `aliBuild build xjalienfs --defaults jalien`

