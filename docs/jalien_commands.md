# jalien Command Reference

### cd
``` 
    usage: cd   [dir]   
```

---

### pwd
---

### mkdir
``` 
    usage: mkdir   [-options] <directory> [<directory>[,<directory>]]
    
    options:
                        -p                     :  create parents as needed
                        -silent                :  execute command silently
```

---

### rmdir
``` 
    usage: rmdir    [<option>] <directory>
    
    options:
                        --ignore-fail-on-non-empty  :    ignore each failure that is solely because a directory is non-empty
                        -p                     :  --parents   Remove DIRECTORY and its ancestors.  E.g., 'rmdir -p a/b/c' is similar to 'rmdir a/b/c a/b a'.
                        -v                     :  --verbose  output a diagnostic for every directory processed
                                               :  --help      display this help and exit
                                               :  --version  output version information and exit
                        -silent                :  execute command silently
```

---

### ls
``` 
    usage: ls   [-options] [<directory>]
    
    options:
                        -l                     :  long format
                        -a                     :  show hidden .* files
                        -F                     :  add trailing / to directory names
                        -b                     :  print in guid format
                        -c                     :  print canonical paths
                        -H                     :  human readable file sizes (1024-based); implies '-l'
```

---

### find
``` 
    usage: find   [flags] <path> <pattern>
    
    
    options:
                        -a                     :  show hidden .* files
                        -s                     :  no sorting
                        -c                     :  print the number of matching files
                        -x <target LFN>        :  create the indicated XML collection with the results of the find operation
                        -d                     :  return also the directories
                        -w[h]                  :  long format, optionally human readable file sizes
                        -j <queueid>           :  filter files created by a certain job ID
                        -l <count>             :  limit the number of returned entries to at most the indicated value
                        -o <offset>            :  skip over the first /offset/ results
                        -r                     :  pattern is a regular expression
                        -f                     :  return all LFN data as JSON fields (API flag only)
                        -y                     :  (FOR THE OCDB) return only the biggest version of each file
```

---

### toXml
``` 
    usage: toXml   [-i] [-x xml_file_name] [-a] [-l list_from] [lfns]
    
    options:
                        -i                     :  ignore missing entries, continue even if some paths are not/no longer available
                        -x                     :  write the XML content directly in this target AliEn file
                        -a                     :  (requires -x) append to the respective collection
                        -l                     :  read the list of LFNs from this file, one path per line
```

---

### cat
``` 
    usage: cat   [-options] [<filename>]
    
    options:
                        -o                     :  outputfilename
                        -n                     :  number all output lines
                        -b                     :  number nonblank output lines
                        -E                     :  shows ends - display $ at end of each line number
                        -T                     :  show tabs -display TAB characters as ^I
```

---

### whereis
``` 
    usage: whereis   [-options] [<filename>]
    
    options:
                        -g                     :  use the lfn as guid
                        -r                     :  resolve links (do not give back pointers to zip archives)
                        -l                     :  lookup the LFN of the ZIP archive (slow and expensive IO operation, only use it sparingly!)
```

---

### cp
``` 
    usage: cp   [-options] <source> [<target>]
    Source and target paths can be one of the following formats:
    	file://<local file on disk>
    	alien://<path in the Grid catalogue>
    	or without a prefix, in which case it's treated as a Grid path
    
    options:
                        -g                     :  treat the Grid path as a GUID
                        -S <SEs|QoS:count>     :  Storage target specifications for uploading, default is 'disk:2'
                        -t                     :  no target needed, create a local temporary file instead, print out the local path at the end
                        -silent                :  execute command silently
                        -w                     :  wait for all replicas to complete upload before returning (default false)
                        -W                     :  do _not_ wait for all replicas to complete upload, return as soon as the first replica is available
                        -T                     :  Use this many concurrent download threads (where possible) - default 1
                        -d                     :  delete local file after a successful upload (i.e. move local to Grid)
                        -j <job ID>            :  the job ID that has created the file
```

!!! warning "client-side implementation"

---

### rm
``` 
    usage: rm    <LFN> [<LFN>[,<LFN>]]
    
    options:
                        -f                     :  ignore nonexistent files, never prompt
                        -r, -R                 :  remove directories and their contents recursively
                        -i                     :  prompt before every removal (for JSh clients)
```

---

### mv
``` 
    usage: mv    <LFN>  <newLFN>
```

---

### touch
``` 
    usage: touch    <LFN> [<LFN>[,<LFN>]]
```

---

### type
``` 
    usage: type   <lfn> 
                        Print the LFN type (file / directory / collection)
```

---

### lfn2guid
``` 
    usage: lfn2guid   <filename>
```

---

### guid2lfn
``` 
    usage: guid2lfn   <GUID>
```

---

### guidinfo
``` 
    usage: guidinfo   <uuid> ...
```

---

### access
``` 
    usage: access   [options] <read|write> <lfn> [<specs>]
                        -s                     :  for write requests, size of the file to be uploaded, when known
                        -m                     :  for write requests, MD5 checksum of the file to be uploaded, when known
                        -j                     :  for write requests, the job ID that created these files, when applicable
                        -f                     :  for read requests, filter the SEs based on the given specs list
                        -u                     :  for read requests, print http(s) URLs where available, and the envelopes in urlencoded format
```

---

### commit
``` 
    usage: commit   API command only, you should not need to call this directly
```

---

### chown
``` 
    Usage: chown -R <user>[.<group>] <file>
    
    Changes an owner or a group for a file
    -R : do a recursive chown
```

---

### chmod
``` 
    Usage: chmod -R <octal mode> <path> [<path>...]
    
    Changes the access mode for a catalogue path
    -R : do a recursive chmod starting from the given path
```

---

### deleteMirror
``` 
    Removes a replica of a file from the catalogue
    Usage:
            deleteMirror [-g] <lfn> <se> [<pfn>]
    
    Options:
       -g: the lfn is a guid
```

---

### md5sum
``` 
    usage: md5sum   <filename1> [<or guid>] ...
```

---

### mirror
``` 
    mirror Copies a file into another SE
     Usage:
    	mirror [-g] [-try <number>] [-S [se[,se2[,!se3[,qos:count]]]]] <lfn> [<SE>]
                     -g:      Use the lfn as a guid
                     -S:     specifies the destination SEs to be used
                     -try <NumOfAttempts>     Specifies the number of attempts to try and mirror the file
```

---

### grep
``` 
    usage: grep   [-linux grep options] <pattern> [<filename>]+
    
    options:
```

---

### changeDiff
``` 
    Show changes between the current version of the file and the previous one (same file name with a '~' suffix)
    usage: changeDiff   [<filename>]
    
    options:
```

---

### listFilesFromCollection
``` 
    usage: listFilesFromCollection   [-options] collection
    
    options:
                        -z                     :  show size and other file details
                        -s                     :  silent (API only)
```

---

### packages
``` 
    usage: packages     list available packages
```

---

### listCEs
``` 
    listCEs: print all (or a subset) of the defined CEs with their details
    usage: listCEs    [-s] [CE name] [CE name] ...
    
    options:
                        -s                     :  print summary information
                        -p                     :  filter by partition names
                        -v                     :  print verbose output (including partitions)
```

---

### jobListMatch
``` 
    jobListMatch: print all the CEs that can run a certain job
    usage: jobListMatch    [jobId]
    
    options:
```

---

### listpartitions
``` 
    listpartitions: print all (or a subset) of the defined partitions
    usage: listpartitions    [-v] [Partition name] [Partition name] ...
    
    options:
                        -v                     :  print verbose output (including member CEs)
```

---

### setCEstatus
``` 
    setCEstatus: Sets the status of a set of Computing Elements
    usage: listpartitions    [-status status] [CE name]  [CE name]  ...
    
    options:
                        -status                :  Status to be set for the CEs (open / locked)
```

---

### submit
``` 
    usage: submit   <URL>
    
                        <URL> => <LFN>
                        <URL> => file:///<local path>
```

---

### ps
``` 
    usage: ps   [-options]
    
    options:
                        -F l | -Fl | -L        :  long output format
                        -f <flags|status>      :  any number of (long or short) upper case job states, or 'a' for all, 'r' for running states, 'f' for failed, 'd' for done, 's' for queued
                        -u <userlist>        
                        -s <sitelist>        
                        -n <nodelist>        
                        -m <masterjoblist>   
                        -o <sortkey>         
                        -j <jobidlist>       
                        -l <query-limit>     
    
                        -M                     :  show only masterjobs
                        -X                     :  active jobs in extended format
                        -A                     :  select all owned jobs of you
                        -W                     :  select all jobs which are waiting for execution of you
                        -E                     :  select all jobs which are in error state of you
                        -a                     :  select jobs of all users
                        -b                     :  do only black-white output
                        -jdl <jobid>           :  display the job jdl
                        -trace <jobid> <tag>*  :  display the job trace information
                        -id                    :  only list the matching job IDs, for batch processing (implies -b)
```

---

### masterjob
``` 
    usage: masterjob   <jobId> [-options] [merge|kill|resubmit|expunge]
    
    options:
                        -status <status>       :  display only the subjobs with that status
                        -id <id>               :  display only the subjobs with that id
                        -site <id>             :  display only the subjobs on that site
                        -printid               :  print also the id of all the subjobs
                        -printsite             :  split the number of jobs according to the execution site
    
                        merge                  :  collect the output of all the subjobs that have already finished
                        kill                   :  kill all the subjobs
                        resubmit               :  resubmit all the subjobs selected
                        expunge                :  delete completely the subjobs
    
                        You can combine kill and resubmit with '-status <status>' and '-id <id>'.
                        For instance, if you do something like 'masterjob <jobId> -status ERROR_IB resubmit',
                         all the subjobs with status ERROR_IB will be resubmitted
```

---

### kill
``` 
    usage: kill   <jobId> [<jobId>[,<jobId>]]
```

---

### w
``` 
    usage: w   Show currently active users on the Grid
```

---

### uptime
``` 
    usage: uptime       
    
    options:
```

---

### resubmit
``` 
    resubmit: resubmits a job or a group of jobs by IDs
            Usage:
                    resubmit <jobid1> [<jobid2>....]
```

---

### top
``` 
    usage: top          
```

---

### registerOutput
``` 
    usage: registerOutput   <job ID>
    
    options:
```

---

### df
``` 
    Shows free disk space
    Usage: df
```

---

### du
``` 
    Gives the disk space usage of one or more directories
    usage: du   [-ncs] <path>
    
    options:
                        -n                     :  Print raw numbers in machine readable format
                        -c                     :  Include collections in the summary information
                        -s                     :  Print a summary of all parameters
```

---

### fquota
``` 
    fquota: Displays information about File Quotas.
    usage: fquota   list [-<options>]
    Options:
      -unit = B|K|M|G: unit of file size
```

---

### jquota
``` 
    usage: jquota   Displays information about Job Quotas.
    
    options:
                        list [username]        :  get job quota information for the current account, or the indicated one
                        set <user> <field> <value>  :  to set quota fileds (one of  maxUnfinishedJobs, maxTotalCpuCost, maxTotalRunningTime)
```

---

### listSEs
``` 
    listSEs: print all (or a subset) of the defined SEs with their details
    usage: listSEs   [-qos filter,by,qos] [-s] [SE name] [SE name] ...
    
    options:
                        -qos                   :  filter the SEs by the given QoS classes. Comma separate entries for 'AND', pass multiple -qos options for an 'OR'
                        -s                     :  print summary information
```

---

### listSEDistance
``` 
    listSEDistance: Returns the closest working SE for a particular site. Usage
    
    options:
                        -site                  :  site to base the results to, instead of using the default mapping of this client to a site
                        -read                  :  use the read metrics, optionally with an LFN for which to sort the replicas. Default is to print the write metrics.
                        -qos                   :  restrict the returned SEs to this particular tag
```

---

### setSite
``` 
    usage: setSite   [site name]
    
    options:
```

---

### testSE
``` 
    Test the functional status of Grid storage elements
    Usage: testSE [options] <some SE names, numbers or @tags>
                        -v                     :  verbose error messages even when the operation is expected to fail
                        -c                     :  show full command line for each test
                        -t                     :  time each operation
                        -a                     :  test all SEs (obviously a very long operation)
```

---

### listTransfer
``` 
    listTransfer: returns all the transfers that are waiting in the system
            Usage:
                    listTransfer [-status <status>] [-user <user>] [-id <queueId>] [-verbose] [-master] [-summary] [-all_status] [-jdl] [-destination <site>]  [-list=<number(all transfers by default)>] [-desc]
```

---

### uuid
``` 
    usage: uuid   <uuid|filename> [<uuid|filename> ...]
    
                        Decode v1 UUIDs and display the interesting bits
```

---

### stat
``` 
    usage: stat   [-v] <filename1> [<or uuid>] ...
                        -v                     :  More details on the status.
```

---

### xrdstat
``` 
    usage: xrdstat   [-d [-i]] [-v] [-p PID,PID,...] [-s SE1,SE2,...] [-c] <filename1> [<or UUID>] ...
    
    options:
                        -d                     :  Check by physically downloading each replica and checking its content. Without this a stat (metadata) check is done only.
                        -i                     :  When downloading each replica, ignore `stat` calls and directly try to fetch the content.
                        -s                     :  Comma-separated list of SE names to restrict the checking to. Default is to check all replicas.
                        -c                     :  Print the full command line in case of errors.
                        -v                     :  More details on the status.
                        -p                     :  Comma-separated list of job IDs to check the input data of
                        -o                     :  Only show the online status (for files with tape replicas in particular)
                        -O                     :  Request the file to be brought online
                        -4                     :  Force IPv4 usage on all commands
                        -6                     :  Force IPv6 usage on all commands
```

---

### resyncLDAP
``` 
    Usage: resyncLDAP
    
                        Synchronizes the DB with the updated values in LDAP
```

---

### optimiserLogs
``` 
    Usage: lastOptimiserLog [-l] [-v] [-f <frequency>] <classnames or metanames>
    
                        Gets the last log from the optimiser
                        -v : Verbose, displays the frequency, last run timestamp and log
                        -l : List the class names that match a query
                        -f <value> : Frequency, in seconds
```

---

### showTagValue
``` 
    usage: showtagValue   [flags] <filename> [<filename>...]
    
    options:
                        -t                     :  restrict to this (comma separated) tag list only (default is to return all available tags)
                        -c                     :  restrict to these (comma separated) list of attributes
                        -l                     :  list available tags only
```

---

### time
``` 
    Usage: time <times>  <command> [command_arguments] 
```

---

### timing
``` 
    usage: timing   [on|off]
    
    options:
                        return server side timing information
```

---

### commandlist
``` 
    usage: commandlist  
```

---

### motd
``` 
    usage: motd   Message of the day/alice/cern.ch/user/a/alienci/alienci
```

---

### ping
``` 
    usage: ping         
                        -c                     :  Number of iterations
                        -s                     :  Sleep between iterations, default 1000 (milliseconds)
                        -n                     :  Numeric IP addresses, don't try to resolve anything
```

---

### version
``` 
    usage: version      
    
                        Get information about the JAliEn version that answers your requests
```

---

### whoami
``` 
    usage: whoami       
    
    options:
                        -v                     :  verbose details of the current identity
```

---

### user
``` 
    usage: user   <user name>
    
                        Change effective role as specified.
```

---

### whois
``` 
    usage: whois   [account name]
    
    options:
                        -s                     :  search for the given string(s) in usernames
                        -f                     :  also search in full names
                        -e                     :  search in email addresses too
                        -d                     :  search in X509 DN (subject) fields
                        -a                     :  search for the given string in all the above fields
```

---

### groups
``` 
    groups [<username>]
    shows the groups current user is a member of.
```

---

### token
``` 
    usage: token   [-options]
    
    options:
                        -u <username>          :  switch to another role of yours
                        -v <validity (days)>   :  default depends on token type
                        -t <tokentype>         :  can be one of: job, jobagent, host, user (default)
                        -jobid <job DN extension>  :  expected to be present in a job token
                        -hostname <FQDN>       :  required for a host certificate
```

---

### lfnexpiretime
``` 
    usage: lfnexpiretime   [-options] [<file>]
    
    options:
                        -r                     :  removes the expire time set for an LFN
                        -a                     :  add a new expire time for the given LFN
                        -e                     :  extends the current expire time for the given LFN
                        -d <number>            :  specifies the number of days in the expire time
                        -w <number>            :  specifies the number of weeks in the expire time
                        -m <number>            :  specifies the number of months in the expire time
                        -y <number>            :  specifies the number of years in the expire time
```

---

